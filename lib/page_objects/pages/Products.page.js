const SaucedemoPage = require('../pages/Saucedemo.page')
const Product = require('../components/Product.component')
const _ = require('underscore')

class ProductsPage extends SaucedemoPage {
    //page locators:
    get header() {return $('.product_label')}
    get products() {return $$('.inventory_item').map(product => new Product(product))}

    //page actions
    getProduct(index) {
        if(index === undefined) {
            return _.sample(this.products)
        } 
        return this.products[index]
    }

    addItemstoCart(numOfItems) {
        let products = this.products.filter(product => product.addRemoveBtn.getText() != 'REMOVE')
        products = _.shuffle(products)
        products = products.slice(0, numOfItems)
        products.forEach(product => {
            product.addRemoveBtn.click()
        })
        return products
    }
}

module.exports = new ProductsPage