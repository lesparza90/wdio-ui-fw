const SaucedemoPage = require('./Saucedemo.page')

class CheckoutPage extends SaucedemoPage {

    get firstNameInput() {return $('#first-name')}
    get lastNameInput() {return $('#last-name')}
    get zipCPInput() {return $('#postal-code')}
    get continueButton() {return $('.cart_button')}
    get errorMessage() {return $('[data-test="error"]')}
}

module.exports = new CheckoutPage