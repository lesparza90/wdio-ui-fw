const SaucedemoPage = require("./Saucedemo.page");
const ProductsPage = require("./Products.page");

class LoginPage extends SaucedemoPage {
    //Page locators:
    get usernameInput() {return $('#user-name')}
    get passwordInput() {return $('#password')}
    get loginBtn() {return $('#login-button')}
    get errorMessage() {return $('[data-test="error"]')}

    //page actions:
    login(username, password) {
        this.usernameInput.setValue(username)
        this.passwordInput.setValue(password)
        this.loginBtn.click()
    }
}

module.exports = new LoginPage()