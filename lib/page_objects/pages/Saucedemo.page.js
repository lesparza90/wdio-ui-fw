const Page = require('./Page')

class SaucedemoPage extends Page {
    //header
    // page locator:
    get hamburgerMenu() {return $('.bm-burger-button button')}
    get logoutBtn() {return $('#logout_sidebar_link')}
    get cartBtn() {return $('[data-icon="shopping-cart"]')}
    get productCounterLbl() {return $('.shopping_cart_badge')}

    //page actions:
    logout(){
        this.hamburgerMenu.click()
        //this.logoutBtn.waitForClickable()
        this.logoutBtn.waitAndClick()
    }

    //footer
}

module.exports = SaucedemoPage