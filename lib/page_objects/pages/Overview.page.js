const SaucedemoPage =require('./Saucedemo.page')
const Product =require('../components/Product.component')

class OverviewPage extends SaucedemoPage {

    get products() {return $$('.cart_item').map(product => new Product(product))}
    get finishBtn() {return $('.cart_button')}
}

module.exports = new OverviewPage