const SaucedemoPage = require('./Saucedemo.page')
const Product = require('../components/Product.component')

class CartPage extends SaucedemoPage {
    get products() {return $$('.cart_item').map(product => new Product(product))}
    get checkoutBtn() {return $('.checkout_button')}
}

module.exports = new CartPage