class Product {
    constructor(element) {
        this.element = element
        this.title = this.titleLbl.getText()
    }

    get titleLbl() {return this.element.$('.inventory_item_name')}
    get addRemoveBtn() {return this.element.$('.btn_inventory')}
}

module.exports = Product