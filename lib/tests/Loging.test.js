const expectChai = require('chai').expect;
const LoginPage = require('../page_objects/pages/Login.page')
const ProductsPage = require('../page_objects/pages/Products.page')
const CartPage = require('../page_objects/pages/Cart.page')
const CheckoutPage = require('../page_objects/pages/Checkout.page')
const OverviewPage = require('../page_objects/pages/Overview.page')
const faker = require('faker')
const testData = require('../test_data/loging.data')

describe('Login application', () => {
    let productsList = []
    let singleProduct

    Object.keys(testData).forEach(user => {
        it('should not loging with invalid credentials', () => {
            LoginPage.open()
            LoginPage.login(testData[user].user, testData[user].passwd)
            expect(LoginPage.errorMessage).toHaveTextContaining('Username and password do not match any user in this service',
            {message: 'Error message was not displayed after trying to login with invalid credentials.', wait:2000})
        });
    })


    it('should login with valid credentials', () => {
        LoginPage.open()
        LoginPage.login(LoginPage.USERNAME, LoginPage.PASSWORD)
        expect(browser).toHaveUrl(`${baseUrl}/inventory.html`)
        expect(ProductsPage.header).toHaveText('Products')
    });

    it('should add a single item to the shopping cart', () => {
        singleProduct = ProductsPage.getProduct()
        singleProduct.addRemoveBtn.click()
        productsList.push(singleProduct.title)
        expect(ProductsPage.productCounterLbl).toHaveText('1')
    });

    it('should multiple items to the shopping cart', () => {
        ProductsPage.addItemstoCart(2).forEach(addedItem => {
            productsList.push(addedItem.title)
        })
        expect(ProductsPage.productCounterLbl).toHaveText('3')
    });

    it('shold navigate to the shoping cart', () => {
        ProductsPage.productCounterLbl.click()
        expect(browser).toHaveUrl(`${baseUrl}/cart.html`)
    });

    it('cart page should have all selected items', () => {
        let actualCartProducts = CartPage.products.map(actualProduct => actualProduct.title)
        productsList.forEach(addedproduct => {
            expectChai(actualCartProducts).to.include(addedproduct);
        })
        CartPage.checkoutBtn.click()
    });

    it('should not continue with missing information', () => {
        CheckoutPage.firstNameInput.setValue(faker.name.firstName())
        CheckoutPage.lastNameInput.setValue(faker.name.lastName())
        CheckoutPage.continueButton.click()
        expect(CheckoutPage.errorMessage).toBeDisplayed()
    });

    it('should continue with the entire information', () => {
        CheckoutPage.zipCPInput.setValue(faker.address.zipCode())
        CheckoutPage.continueButton.click()
        expect(browser).toHaveUrl(`${baseUrl}/checkout-step-two.html`)
    });

    it('Final order should have the added items', () => {
        let actualProducts = OverviewPage.products.map(product => product.title)
        productsList.forEach(expectedProduct => {
            expectChai(actualProducts).to.include(expectedProduct)
        })
    });

    it('should finish order', () => {
        OverviewPage.finishBtn.click()
        expect(browser).toHaveUrl(`${baseUrl}/checkout-complete.html`)
    });

    it('should logout from product page', () => {
        LoginPage.logout()
        expect(LoginPage.loginBtn).toBeDisplayed()
    });
});