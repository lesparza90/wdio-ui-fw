module.exports = {
    envURL: function() {
        if(process.env.HOSTNAME) {
            return process.env.HOSTNAME
        } else {
            return 'https://www.saucedemo.com'
        }
    }
}